
export const AppConfigs = {
    "rootTemplate": "/Template/Application.xml",
    "templateRequest":"templateRequest",
    "Auth":{
        "type":"sso2"
    },
    "API": {
        //"origin":"https://etrn-x5-tst.esphere.ru",
        //"origin":"http://api-client-etrn-x5-test.apps.oshift.test.bsd.esphere.local/",
        //"origin": "http://95.165.27.251:9080",
        //"origin": "",
        "origin": "http://95.165.27.251:9080",
        "template": "",
    } as { [key:string]:string }
};

/*
export const AppConfigs = {
    "rootTemplate": "/Template/Root.xml",
    "templateRequest":"templateRequest",
    "Auth":{
        "type":"sso2"
    },
    "API": {
        //"origin":"https://etrn-x5-tst.esphere.ru",
        //"origin":"http://api-client-etrn-x5-test.apps.oshift.test.bsd.esphere.local/",
        "origin": "http://95.165.27.251:9080",
        "template": "",
    } as { [key:string]:string }
};
 */
// export const AppConfigs = {
//     "rootTemplate": "/Template/Root.xml",
//     "templateRequest":"templateRequest",
//     "Auth":{
//         "type":"sso"
//     },
//     "API": {
//         "origin":"http://etrn-x.esphere.ru:8080",
//         "template": "",
//     } as { [key:string]:string }
// };

//PROD
// export const AppConfigs = {
//     "rootTemplate": "/Template/Root.xml",
//     "templateRequest":"templateRequest",
//     "API": {
//         "origin":"",
//         "auth":"/auth/access/process",
//         "template": "",
//     } as { [key:string]:string }
// };
