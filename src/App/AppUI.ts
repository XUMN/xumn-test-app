import {TCoreUI,launchCoreUI} from "../Core/CoreUI";

export const AppUI = {
    "CertificateProvider": require("./UI/Сertificate/CertificateProvider"),
    "FileSignProvider": require("./UI/Сertificate/FileSignProvider"),
} as TCoreUI;

export const launchAppUI = () => launchCoreUI(AppUI);