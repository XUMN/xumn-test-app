import React from 'react';
import {AppConfigs} from "./AppConfigs";
import {launchAppRequest} from "./AppRequest";
import {launchAppCommand} from "./AppCommand";
import {launchAppUI} from "./AppUI";
import {TemplateProvider} from "../Core/UI/Provider/Template/TemplateProvider";

const App = () => {
    launchAppRequest();
    launchAppCommand();
    launchAppUI();
    return (
        <TemplateProvider source={AppConfigs.rootTemplate}/>
    );
};

export default App;
