
import getCadespluginAPI from "async-cadesplugin";
//import {getCert} from "async-cadesplugin/src/сertificatesApi";
import Certificate from 'pkijs/src/Certificate';
import {fromBER} from 'asn1js';



const X509_CN_O = "2.5.4.10";
const X509_CN_T = "2.5.4.12";
const X509_CN_G = "2.5.4.42";
const X509_CN_SN = "2.5.4.4";

function _base64ToArrayBuffer(base64) {
    let binary_string = window.atob(base64);
    let len = binary_string.length;
    let bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

export const requestAvailableCertificate = async () => {


    try {
        const api = await getCadespluginAPI();
        const certificate = await api.getCertsList();


        const list = [];
        for( const e of certificate ) {
            e["person"] = {};

            let certContent = await e.certApi.Export(0);
            try {
                const asn1 = fromBER(_base64ToArrayBuffer(certContent));
                const certificate = new Certificate({ schema: asn1.result });

                // Search the subject's attributes for the common name of the certificate
                const subjectAttributes = certificate.subject.typesAndValues;
                for (let index = 0; index < subjectAttributes.length; index++) {
                    const attribute = subjectAttributes[index];

                    if (attribute.type === X509_CN_SN){
                        e["person"]["familyName"] = attribute.value.valueBlock.value;
                    }

                    if (attribute.type === X509_CN_G){
                        const t = attribute.value.valueBlock.value.split(" ");
                        e["person"]['firstName'] = t[0];
                        e["person"]['patronymic'] = t.length > 1 ? t[1] : undefined;
                    }

                    if (attribute.type === X509_CN_T){
                        e["position"] = attribute.value.valueBlock.value;
                    }

                    if (attribute.type === X509_CN_O){
                        e["organization"] = attribute.value.valueBlock.value;
                    }
                }
                console.log(e);
                list.push(e);
            }
            catch (error) {
                console.error('Error parsing certificate:', error);
            }
        }

        return list;
    } catch (error) {
        return undefined
    }

};

export const SignFile = async (data, thumbprint) => {
    const api = await getCadespluginAPI();
    data.name = data.name.replace('.xml','.bin');
    data.content = await api.signBase64( thumbprint, String(data.content) );
    // data.content = "MIINQQYJKoZIhvcNAQcCoIINMjCCDS4CAQExDjAMBggqhQMHAQECAgUAMAsGCSqGSIb3DQEHAaCC\n" +
    // "CJswggiXMIIIRKADAgECAhECFuuQAFqskrxGEuHiZgfXCDAKBggqhQMHAQEDAjCCAYYxHjAcBgkq\n" +
    // "hkiG9w0BCQEWD2hlbHBAZXNwaGVyZS5ydTEYMBYGBSqFA2QBEg0xMDU3ODEyNzUyNTAyMRowGAYI\n" +
    // "KoUDA4EDAQESDDAwNzgwMTM5MjI3MTELMAkGA1UEBhMCUlUxLTArBgNVBAgMJDc4INCzLiDQodCw\n" +
    // "0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQszEmMCQGA1UEBwwd0KHQsNC90LrRgi3Qn9C10YLQtdGA\n" +
    // "0LHRg9GA0LMxTDBKBgNVBAkMQ9CR0L7Qu9GM0YjQvtC5INCh0LDQvNC/0YHQvtC90LjQtdCy0YHQ\n" +
    // "utC40Lkg0L/RgC4g0LQuIDY4INC70LjRgi4g0J0xMDAuBgNVBAsMJ9Cj0LTQvtGB0YLQvtCy0LXR\n" +
    // "gNGP0Y7RidC40Lkg0YbQtdC90YLRgDE2MDQGA1UECgwt0J7QntCeINCa0J7QoNCj0KEg0JrQvtC9\n" +
    // "0YHQsNC70YLQuNC90LMg0KHQndCTMRIwEAYDVQQDDAlDQSBTdHJvbmcwHhcNMjAxMDIwMDgzNzM4\n" +
    // "WhcNMjExMDIwMDg0NzM4WjCCAhAxITAfBgkqhkiG9w0BCQEWElNCRVJLT1JVU0BLT1JVUy5ydTEW\n" +
    // "MBQGBSqFA2QDEgs2ODc1NzI2NTk3NTEYMBYGBSqFA2QBEg0xMTE1MDI0MDA0MjI2MRowGAYIKoUD\n" +
    // "A4EDAQESDDAwNTAyNDEyMTQ2OTELMAkGA1UEBhMCUlUxJzAlBgNVBAgMHjUwINCc0L7RgdC60L7Q\n" +
    // "stGB0LrQsNGPINC+0LHQuzEqMCgGA1UEBwwh0JDQktCi0J7QlNCe0KDQntCT0JAg0JHQkNCb0KLQ\n" +
    // "mNCvMScwJQYDVQQKDB7QntCe0J4gItCU0JDQndCe0J0g0KLQoNCV0JnQlCIxJzAlBgNVBAMMHtCe\n" +
    // "0J7QniAi0JTQkNCd0J7QnSDQotCg0JXQmdCUIjFcMFoGA1UECQxT0JrQmNCb0J7QnNCV0KLQoCAy\n" +
    // "NizQkdCY0JfQndCV0KEt0KbQldCd0KLQoCAn0KDQmNCT0JAg0JvQldCd0JQnLNCh0KLQoNCe0JXQ\n" +
    // "ndCY0JUg0JIxODA2BgNVBAwML9Cg0YPQutC+0LLQvtC00LjRgtC10LvRjCDQvdCw0L/RgNCw0LLQ\n" +
    // "u9C10L3QuNGPMTIwMAYDVQQqDCnQktCw0YDQstCw0YDQsCDQkNC70LXQutGB0LDQvdC00YDQvtCy\n" +
    // "0L3QsDEdMBsGA1UEBAwU0JHQtdGA0LTRjtC20LXQvdC60L4wZjAfBggqhQMHAQEBATATBgcqhQMC\n" +
    // "AiQABggqhQMHAQECAgNDAARAi9RjjP5u1FsLV/ZQPZcyDErRus5WG6mKb1s2JLyBTcfO/K5H0OqN\n" +
    // "qPihbbD9jIjJuxrU+bd1sUV2eQdUMF6KiqOCA/YwggPyMA4GA1UdDwEB/wQEAwIE8DAsBgNVHSUE\n" +
    // "JTAjBggrBgEFBQcDBAYIKwYBBQUHAwIGBFUdJQAGByqFAwICIgYwNQYJKwYBBAGCNxUHBCgwJgYe\n" +
    // "KoUDAgIyAQmDldhrhbzLZYTdkEKD5Z9+g8VhgsByAgEBAgEAMB0GA1UdDgQWBBSQ17SFECOh7E6+\n" +
    // "V/UxSUpmDKYouTBHBggrBgEFBQcBAQQ7MDkwNwYIKwYBBQUHMAKGK2h0dHA6Ly90ZXN0LmVzcGhl\n" +
    // "cmUucnUvcm9vdC1zdHJvbmcvcm9vdC5jcnQwHQYDVR0gBBYwFDAIBgYqhQNkcQEwCAYGKoUDZHEC\n" +
    // "MCsGA1UdEAQkMCKADzIwMjAxMDIwMDgzNzM4WoEPMjAyMTEwMjAwODM3MzhaMIGiBgUqhQNkcASB\n" +
    // "mDCBlQwpItCa0YDQuNC/0YLQvtCf0YDQviBDU1AiINCy0LXRgNGB0LjRjyA0LjAMKiLQmtGA0LjQ\n" +
    // "v9GC0L7Qn9GA0L4g0KPQpiIg0LLQtdGA0YHQuNC4IDIuMAwd0KHQpC8xMjQtMjg2NCDQvtGCIDIw\n" +
    // "LjAzLjIwMTYMHdCh0KQvMTI4LTI4ODEg0L7RgiAxMi4wNC4yMDE2MCMGBSqFA2RvBBoMGCLQmtGA\n" +
    // "0LjQv9GC0L7Qn9GA0L4gQ1NQIjBiBgNVHR8EWzBZMFegVaBThlFodHRwOi8vdGVzdC5lc3BoZXJl\n" +
    // "LnJ1L3Rlc3R1Yy1zdHJvbmcvMmEzYWIyMDYyNWJhY2RkZjM1ODM0NWQ5MzMxNGVkNWUzZDhkZDc1\n" +
    // "MS5jcmwwggGXBgNVHSMEggGOMIIBioAUKjqyBiW6zd81g0XZMxTtXj2N11GhggFepIIBWjCCAVYx\n" +
    // "ITAfBgkqhkiG9w0BCQEWEiBpbmZvQGNyeXB0b3Byby5ydTEYMBYGBSqFA2QBEg0xMDM3NzAwMDg1\n" +
    // "NDQ0MRowGAYIKoUDA4EDAQESDDAwNzcxNzEwNzk5MTELMAkGA1UEBhMCUlUxGDAWBgNVBAgMDzc3\n" +
    // "INCc0L7RgdC60LLQsDEVMBMGA1UEBwwM0JzQvtGB0LrQstCwMS8wLQYDVQQJDCbRg9C7LiDQodGD\n" +
    // "0YnRkdCy0YHQutC40Lkg0LLQsNC7INC0LiAxODElMCMGA1UECgwc0J7QntCeICLQmtCg0JjQn9Ci\n" +
    // "0J4t0J/QoNCeIjFlMGMGA1UEAwxc0KLQtdGB0YLQvtCy0YvQuSDQs9C+0LvQvtCy0L3QvtC5INCj\n" +
    // "0KYg0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIiDQk9Ce0KHQoiAyMDEyICjQo9CmIDIuMCmC\n" +
    // "EE6kVQHAqf6cQWpBjxGBkbQwCgYIKoUDBwEBAwIDQQCzCD+J0yzSvpqdN2DvmfUwRF9pgg7kDnco\n" +
    // "nvLjDgngyzpNFpTaeMr+Iwpg+a9zc2pkGy++OgKOYjCGA/I406OPMYIEazCCBGcCAQEwggGdMIIB\n" +
    // "hjEeMBwGCSqGSIb3DQEJARYPaGVscEBlc3BoZXJlLnJ1MRgwFgYFKoUDZAESDTEwNTc4MTI3NTI1\n" +
    // "MDIxGjAYBggqhQMDgQMBARIMMDA3ODAxMzkyMjcxMQswCQYDVQQGEwJSVTEtMCsGA1UECAwkNzgg\n" +
    // "0LMuINCh0LDQvdC60YIt0J/QtdGC0LXRgNCx0YPRgNCzMSYwJAYDVQQHDB3QodCw0L3QutGCLdCf\n" +
    // "0LXRgtC10YDQsdGD0YDQszFMMEoGA1UECQxD0JHQvtC70YzRiNC+0Lkg0KHQsNC80L/RgdC+0L3Q\n" +
    // "uNC10LLRgdC60LjQuSDQv9GALiDQtC4gNjgg0LvQuNGCLiDQnTEwMC4GA1UECwwn0KPQtNC+0YHR\n" +
    // "gtC+0LLQtdGA0Y/RjtGJ0LjQuSDRhtC10L3RgtGAMTYwNAYDVQQKDC3QntCe0J4g0JrQntCg0KPQ\n" +
    // "oSDQmtC+0L3RgdCw0LvRgtC40L3QsyDQodCd0JMxEjAQBgNVBAMMCUNBIFN0cm9uZwIRAhbrkABa\n" +
    // "rJK8RhLh4mYH1wgwDAYIKoUDBwEBAgIFAKCCAmEwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc\n" +
    // "BgkqhkiG9w0BCQUxDxcNMjAxMDIxMTUzNTEwWjAvBgkqhkiG9w0BCQQxIgQghDPkiCQ0Mpb2DWCJ\n" +
    // "XtF1sEF8hibc6YorpyJtHaeHJ34wggH0BgsqhkiG9w0BCRACLzGCAeMwggHfMIIB2zCCAdcwCgYI\n" +
    // "KoUDBwEBAgIEICse9v8TS32DsPIUyArNGzzliNjY7SNLtte/k0JByx1aMIIBpTCCAY6kggGKMIIB\n" +
    // "hjEeMBwGCSqGSIb3DQEJARYPaGVscEBlc3BoZXJlLnJ1MRgwFgYFKoUDZAESDTEwNTc4MTI3NTI1\n" +
    // "MDIxGjAYBggqhQMDgQMBARIMMDA3ODAxMzkyMjcxMQswCQYDVQQGEwJSVTEtMCsGA1UECAwkNzgg\n" +
    // "0LMuINCh0LDQvdC60YIt0J/QtdGC0LXRgNCx0YPRgNCzMSYwJAYDVQQHDB3QodCw0L3QutGCLdCf\n" +
    // "0LXRgtC10YDQsdGD0YDQszFMMEoGA1UECQxD0JHQvtC70YzRiNC+0Lkg0KHQsNC80L/RgdC+0L3Q\n" +
    // "uNC10LLRgdC60LjQuSDQv9GALiDQtC4gNjgg0LvQuNGCLiDQnTEwMC4GA1UECwwn0KPQtNC+0YHR\n" +
    // "gtC+0LLQtdGA0Y/RjtGJ0LjQuSDRhtC10L3RgtGAMTYwNAYDVQQKDC3QntCe0J4g0JrQntCg0KPQ\n" +
    // "oSDQmtC+0L3RgdCw0LvRgtC40L3QsyDQodCd0JMxEjAQBgNVBAMMCUNBIFN0cm9uZwIRAhbrkABa\n" +
    // "rJK8RhLh4mYH1wgwDAYIKoUDBwEBAQEFAARAdPgjJnDvaFq5Ljl43XiyhIbI0AGn3wKu6d7pTqUr\n" +
    // "LNOAudax4Vl5qKCD/onqgIxTnubbkFRTD4xIK15a6EwdtQ==";
    return data;
};