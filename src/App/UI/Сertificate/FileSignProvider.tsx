import React, {FunctionComponent, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../Core/CoreBinding";
import {SignFile} from "./CertificatePlugin";

export type ContentServiceType = {
    thumbprint:string;
    origin:string;
    result:string;
}

export const CertificateProvider:FunctionComponent<ContentServiceType> = observer((props) => {
    const [state, setState] = useState(false);
    useEffect(() => {
        const origin = new CoreBinding().obtainValue(props.origin).bindingValue;
        const thumbprint = new CoreBinding().obtainValue(props.thumbprint).bindingValue;
        SignFile(JSON.parse(JSON.stringify(origin)), thumbprint).then( (result) =>{
            new CoreBinding().recordValue(props.result,result);
            setState(true);
        });

    }, [props]);

    return state ? <>{props.children}</> : null;
});

export default CertificateProvider;