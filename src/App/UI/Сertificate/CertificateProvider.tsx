import React, {FunctionComponent, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../Core/CoreBinding";
import {requestAvailableCertificate} from "./CertificatePlugin";

export type ContentServiceType = {
    binding: string,
}

export const CertificateProvider:FunctionComponent<ContentServiceType> = observer((props) => {
    const [state, setState] = useState(false);
    useEffect(() => {
        requestAvailableCertificate().then( (e) =>{
            console.log( e )
            new CoreBinding().recordValue(props.binding,e);
            setState(true);
        });
    }, [props]);
    return state ? <>{props.children}</> : null;
});

export default CertificateProvider;