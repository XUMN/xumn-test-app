import { getUserCertificates, Certificate } from 'crypto-pro';

(async () => {
    let certificates: Certificate[];

    try {
        certificates = await getUserCertificates();
    } catch(error) {
        // ...
    }
})();